function goBack() {
    window.history.back();
}

//brand-mobile redirect to respective phone pages

//for oneplus mobile
var oneplus = document.querySelectorAll('.oneplus > .brand-mobile');

for (var i = 0; i < oneplus.length; i++) {
    oneplus[i].addEventListener('click', function(event) {
        window.location.href = "oneplus_demoPhone.html";
    });
}

//for oneplus mobile
var iphone = document.querySelectorAll('.iphone > .brand-mobile');

for (var i = 0; i < iphone.length; i++) {
    iphone[i].addEventListener('click', function(event) {
        window.location.href = "iphone_demoPhone.html";
    });
}

//for oneplus mobile
var samsung = document.querySelectorAll('.samsung > .brand-mobile');

for (var i = 0; i < samsung.length; i++) {
    samsung[i].addEventListener('click', function(event) {
        window.location.href = "samsung_demoPhone.html";
    });
}


//for option of color and size

var size = document.querySelector(".actualSize");
document.querySelectorAll('.size-selection').forEach(item => {
    item.addEventListener('click', event => {
        const clickedTarget = event.target;
        clickedTarget.classList.add('selected');
        var actualValue = clickedTarget.value;
        // console.log(clickedTarget);
        size.innerHTML = actualValue;
    })
});


var color = document.querySelector(".actualColor");
document.querySelectorAll('.color-selection').forEach(item => {
    item.addEventListener('click', event => {
        const clickedTarget = event.target;
        var actualValue = clickedTarget.value;
        color.innerHTML = actualValue;

        // console.log(actualValue);
        console.log(clickedTarget.parentNode.children.length);

        //removing class 'selected from child element(button)'
        for (i = 0; i < clickedTarget.parentNode.children.length; i++) {
            clickedTarget.parentNode.children[i].classList.remove('selected');
        }

        //adding class selected for selected item
        clickedTarget.classList.add('selected');

    })
});

//for rating

// var ratingItem = document.querySelector(".rating");
// ratingItem.addEventListener('click', rate);

// function rate(e) {
//     const clickedStar = e.target;

//     if (clickedStar.className == "rating-text") {

//     }
//     else {
//         console.log(clickedStar.children);

//         clickedStar.classList.add("rated");
//     }

// }



//for quantity change

//quantity
var quantity = document.getElementById('quantity-value').value;

//item cost
var itemAmount = document.getElementById('amount').innerHTML;
var floatValueTotal = parseFloat(itemAmount.replace('$', ''));

//total cost
var totalCost = document.getElementById('total-cost');

function calculateTotal() {
    var total = quantity * floatValueTotal;
    totalCost.innerHTML = "$" + total;
}

calculateTotal();

var subtract = document.querySelector('.sub');
var add = document.querySelector('.add');

subtract.addEventListener('click', subtraction);
add.addEventListener('click', addition);

function subtraction(event) {

    if (quantity > 1) {
        quantity -= 1;
        document.getElementById('quantity-value').value = quantity;
        console.log(quantity);

        calculateTotal();
    }
    else {
        document.getElementById('quantity-value').value = quantity;
    }

}

function addition(event) {
    quantity++;
    console.log(quantity);
    document.getElementById('quantity-value').value = quantity;
    calculateTotal();
}


//for add to cart and buy now

var cart = document.getElementById('cart');
var productName = document.getElementById('product-name');

//for cart product, quantity and cost
var cartItemName = document.getElementById('cart-item-name');
var cartItemQuantity = document.getElementById('cart-item-quantity');
var cartItemCost = document.getElementById('cart-item-cost');

//for image in purchase
var mainImageLink = document.querySelector('.main-image-holder img');
var purchaseImage = document.querySelector('.cart-item .img-wrap img');

cart.addEventListener('click', function () {
    alert( cartItemQuantity.innerHTML + " pieces of " + cartItemName.innerHTML + " added to cart which will cost " + totalCost.innerHTML);
});

var buy = document.getElementById('buy-now');
buy.addEventListener('click', purchaseAlert);

function showAlert() {
    document.getElementById('alert-overlay').style.display = "block";
    document.getElementById('alert-holder').style.display = "block";
}

function hideAlert() {
    document.getElementById('alert-overlay').style.display = "none";
    document.getElementById('alert-holder').style.display = "none";
}

function purchaseAlert(event) {
    //setting value of product
    cartItemName.innerHTML = productName.innerHTML;
    cartItemQuantity.innerHTML = quantity;
    cartItemCost.innerHTML = totalCost.innerHTML;

    //setting product image
    purchaseImage.setAttribute('src', mainImageLink.src);

    showAlert();
}

//for canceling the purchase
var cancelBtn = document.querySelectorAll('.cancel');
cancelBtn.forEach(item => {
    item.addEventListener('click', event => {

        for (i = 0; i < cancelBtn.length; i++) {
            hideAlert();
        }
    })
});

//for successful purchase
var confirm = document.querySelector('.confirm');

confirm.addEventListener('click', function () {
    hideAlert();
    alert("Thank you for your purchase !!");
});